///////////////////////////////////////////////
// Required
///////////////////////////////////////////////
var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    merge = require('gulp-merge'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    clean = require('gulp-clean'),
    del = require('del'),
    cssmin = require('gulp-cssmin'),
    debug = require('gulp-debug'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

//Application Location
var appLocation = 'app';

//Where Bower Components Are
var bowerComponents = appLocation + '/bower_components';

//Where Application Should Publish To
var publishLocation = 'public';

///////////////////////////////////////////////
// Clean task
///////////////////////////////////////////////
gulp.task('clean', function(){
  del([
		publishLocation + '/**'
	]);
});

///////////////////////////////////////////////
// Scripts task
///////////////////////////////////////////////
gulp.task('scripts', function(){
  var scripts = gulp.src([appLocation +'/js/**/*.js', '!'+appLocation+'/js/**/*.min.js'])
  .pipe(rename({suffix:'.min'}))
  .pipe(uglify())
  .pipe(gulp.dest(publishLocation + '/js'))
  .pipe(reload({stream:true}));

  var angularScripts = gulp.src([
    appLocation + '/config.js',
    appLocation + '/main.js',
    appLocation + '/directives/**/*.js',
    appLocation + '/models/**/*.js',
    appLocation + '/services/**/*.js',
    appLocation + '/appController.js',
    appLocation + '/modules/**/*.js',
  ])
  .pipe(concat('angular-script.js'))
  //.pipe(rename({suffix:'.min'}))
  //.pipe(uglify())
  .pipe(gulp.dest(publishLocation + '/'))
  .pipe(reload({stream:true}));

   return merge(scripts, angularScripts);
});

///////////////////////////////////////////////
// HTML task
///////////////////////////////////////////////
gulp.task('html', function(){
  var index = gulp.src(appLocation + '/index.html')
  .pipe(gulp.dest(publishLocation + '/'))
  .pipe(debug({title:'dest:'}))
  .pipe(reload({stream:true}));

  var directives = gulp.src(appLocation + '/directives/**/*.html')
  .pipe(gulp.dest(publishLocation + '/'))
  .pipe(debug({title:'dest:'}))
  .pipe(reload({stream:true}));

  var moduels = gulp.src(appLocation + '/**/*.html')
  .pipe(gulp.dest(publishLocation + '/'))
  .pipe(debug({title:'dest:'}))
  .pipe(reload({stream:true}));

  return merge(index, directives, moduels);
});

///////////////////////////////////////////////
// css task
///////////////////////////////////////////////
gulp.task('css', function(){
  gulp.src([appLocation + '/css/**/*.css',
            '!' + appLocation + '/css/**/*.min.css'])
  .pipe(cssmin())
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest(publishLocation + '/css/'))
  .pipe(reload({stream:true}));
});

///////////////////////////////////////////////
// css task
///////////////////////////////////////////////
gulp.task('images', function(){
  gulp.src(appLocation + '/images/**/*')
  .pipe(gulp.dest(publishLocation + '/images/'));
});

///////////////////////////////////////////////
// publish bower_components
///////////////////////////////////////////////
gulp.task('copyBowerComponents', function(){
  var copyJs = gulp.src([bowerComponents + '/angular/angular.min.js',
            bowerComponents + '/jquery/dist/jquery.min.js',
            bowerComponents + '/angular-ui-router/release/angular-ui-router.min.js',
            bowerComponents + '/bootstrap/dist/js/bootstrap.min.js',
            bowerComponents + '/a0-angular-storage/dist/angular-storage.min.js',
            bowerComponents + '/angular-bootstrap/ui-bootstrap-tpls.min.js',
            bowerComponents + '/jquery-ui/jquery-ui.min.js',
            bowerComponents + '/ng-file-upload/ng-file-upload.min.js',
            bowerComponents + '/at-table/dist/angular-table.min.js',
            bowerComponents + '/angucomplete-alt/angucomplete-alt.js',
            bowerComponents + '/moment/min/moment-with-locales.min.js',
            bowerComponents + '/angular-moment/angular-moment.min.js',
            bowerComponents + '/angular-gantt/dist/angular-gantt.min.js',
            bowerComponents + '/angular-gantt/dist/angular-gantt-plugins.min.js',
            bowerComponents + '/angular-gantt/dist/angular-gantt-table-plugin.min.js',
            bowerComponents + '/angular-ui-tree/dist/angular-ui-tree.min.js',
            bowerComponents + '/angular-gantt/dist/angular-gantt-tree-plugin.min.js'
          ],{base: './' + appLocation + '/'})
  .pipe(gulp.dest(publishLocation));

  var copyCss = gulp.src([bowerComponents + '/bootstrap/dist/css/bootstrap.min.css',
                          bowerComponents + '/bootstrap/dist/fonts/*',
                          bowerComponents + '/angucomplete-alt/angucomplete-alt.css',
                          bowerComponents + '/angular-gantt/dist/angular-gantt.min.css',
                          bowerComponents + '/angular-gantt/dist/angular-gantt-plugins.min.css',
                          bowerComponents + '/angular-gantt/dist/angular-gantt-table-plugin.min.css',
                          bowerComponents + '/angular-ui-tree/dist/angular-ui-tree.min.css',
                          bowerComponents + '/angular-gantt/dist/angular-gantt-tree-plugin.min.css'
                        ],{base: './' + appLocation + '/'})
  .pipe(gulp.dest(publishLocation));
});

///////////////////////////////////////////////
// Broswer-Sync plugin
///////////////////////////////////////////////
gulp.task('browser-sync', function(){
  browserSync({
    server:{
      baseDir:'./'+publishLocation+'/'
    }
  });
});

///////////////////////////////////////////////
// Watch task
///////////////////////////////////////////////
gulp.task('watch', function(){
  gulp.watch(appLocation + '/js/**/*.js', ['scripts']);
  gulp.watch(appLocation + '/*.js', ['scripts']);
  gulp.watch(appLocation + '/directives/**/*.js', ['scripts']);
  gulp.watch(appLocation + '/models/**/*.js', ['scripts']);
  gulp.watch(appLocation + '/modules/**/*.js', ['scripts']);
  gulp.watch(appLocation + '/services/**/*.js', ['scripts']);
  gulp.watch(appLocation + '/css/**/*.css', ['css']);
  gulp.watch(appLocation + '/**/*.html', ['html'])
});

///////////////////////////////////////////////
// Default task
///////////////////////////////////////////////
gulp.task('default', ['scripts',
                      'css',
                      'images',
                      'html',
                      'browser-sync',
                      'copyBowerComponents',
                      'watch']);
