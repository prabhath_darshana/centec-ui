var config_module = angular.module('config', []);

var config_data = {
  config:{
    APP_NAME:'CentecERP',
    APP_VERSION:'1.0-beta',
    SERVICE_URL:'http://localhost/centec/centec-service/public/',
    // SERVICE_URL:'https://192.168.1.2/centec/centec-service/public/',
    X_TOKEN:'X-Token',
    USER:'user',
    APP_DATE_FORMAT:'yyyy-MM-dd',
    URL_APPENDER:"#!",
    STATUS:{
      SUCCESS:'success',
      FAILED:'failed'
    },
    APP_TABLE_CONFIG:{
      itemsPerPage: 5,
      fillLastPage: true
    },
    SUCCESS_MSG:{
      CREATE_ITEM:'Created Successfully',
      UPDATE_ITEM:'Updated Successfully',
      DELETE_ITEM:'Deleted Successfully',
    },
    ERROR_MSG:{
      CREATE_ITEM:'Failed to Create',
      UPDATE_ITEM:'Failed to Update',
      DELETE_ITEM:'Failed to Delete',
      FIELD_REQ_EMAIL:'Email must be entered.',
      FIELD_REQ_PASSWORD:'Password must be entered.',
      FIELD_REQ_PROJECT:'Project required',
      FIELD_REQ_DATE:'Date required',
      FIELD_REQ_DESCRIPTION:'Description required',
      AUTH_ERROR:'Authentication problem. Signin first.',
      SYSTEM_FAILUER:'Internal server error. Please try again later.',
      SYSTEM_FAILUER_UNKNOWN:'Unkown error occurred. Contact system administrator',
    }
  }
}

angular.forEach(config_data, function (key, value) {
    config_module.constant(value, key);
});
