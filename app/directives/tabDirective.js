app.directive('uiTabs', function() {
  return {
    restrict: 'A',
    link: function(scope, elm, attrs) {
      var jqueryElm = $(elm[0]);
      $(jqueryElm).tabs()
    }
  };
})
