app.directive('inputSuggest', function(){
  return {
    restrict:'E',
    templateUrl:'inputsuggest/inputsuggest.html',
    scope:{
      label:'@',
      type:'@',
      id:'@',
      placeholder:"@",
      data:'=',
      error:"=?",
      errorMsg:"=",
      required:"=?",
      value:"=",
      init:"=",
      desabled:"=?"
    },
    controller:['$scope', function($scope){

    }],
  }
});
