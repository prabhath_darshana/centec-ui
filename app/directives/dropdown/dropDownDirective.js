app.directive('dropDown', function(){
  return {
    restrict: 'E',
    templateUrl:'dropdown/dropdown.html',
    scope:{
      label:'@',
      id:'@',
      error:"=?",
      errorMsg:"=",
      required:"=?",
      value:"=",
      data:"=",
      tab:"@",
      ngDisabled:"=?"
    },
    controller:['$scope', function($scope){
      $scope.change = function(){
        $scope.error = undefined;
      }
    }],
  }
})
