app.directive('datepicker', function(){
  return{
    restrict:'E',
    templateUrl:'datepicker/datepicker.html',
    scope:{
      label:'@',
      required:'=?',
      value:"=",
      id:'@',
      error:"=?",
      errorMsg:"=",
    },
    controller:['$scope','config', function($scope, config){
      $scope.change = function(){
        $scope.error = false;
      }

       $scope.clear = function() {
         $scope.dt = null;
       };

       $scope.inlineOptions = {
         customClass: getDayClass,
         minDate: new Date(),
         showWeeks: true
       };

       $scope.dateOptions = {
        //  dateDisabled: disabled,
         formatYear: 'yyyy',
        //  maxDate: new Date(2026, 5, 22),
        //  minDate: new Date(),
         startingDay: 1
       };

       // Disable weekend selection
       function disabled(data) {
         var date = data.date,
           mode = data.mode;
         return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
       }

       $scope.toggleMin = function() {
         $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
         $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
       };
       $scope.toggleMin();

       $scope.open = function() {
         $scope.popup1.opened = true;
       };

      //  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
       $scope.format = config.APP_DATE_FORMAT;//$scope.formats[1];
      //  $scope.altInputFormats = ['d!/M!/yyyy'];

       $scope.popup1 = {
         opened: false
       };

       function getDayClass(data) {
         var date = data.date,
           mode = data.mode;
         if (mode === 'day') {
           var dayToCheck = new Date(date).setHours(0,0,0,0);

           for (var i = 0; i < $scope.events.length; i++) {
             var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

             if (dayToCheck === currentDay) {
               return $scope.events[i].status;
             }
           }
         }

         return '';
       }
    }],
  }
});
