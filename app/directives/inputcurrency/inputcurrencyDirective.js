app.directive('inputCurrency', function(){
  return{
    restrict:'E',
    templateUrl:'inputcurrency/inputcurrency.html',
    scope:{
      label:'@',
      id:'@',
      placeholder:"@",
      error:"=?",
      errorMsg:"=",
      required:"=?",
      value:"=",
      tab:"@"
    },
    controller:['$scope', function($scope){

    }],
  }
})

app.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value);
      });
    }
  };
});
