app.directive('inputFile', function(){
  return {
    restrict:'E',
    templateUrl:'inputfile/inputfile.html',
    scope:{
      label:'@',
      id:'@',
      placeholder:"@",
      error:"=?",
      errorMsg:"=",
      required:"=?",
      value:"=",
    },
    controller:['$scope', function($scope){
      $scope.setFile = function(file){
        $scope.value = file;
      }
    }],
  }
});
