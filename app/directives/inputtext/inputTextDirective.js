app.directive('inputText', function(){
  return {
    restrict:'E',
    templateUrl:'inputtext/inputText.html',
    scope:{
      label:'@',
      type:'@',
      id:'@',
      placeholder:"@",
      error:"=?",
      errorMsg:"=",
      required:"=?",
      value:"=",
      pattern:"@",
      tab:"@",
      disable:"=?",
    },
    controller:['$scope', function($scope){
      switch($scope.type){
        case 'tel':
          $scope.pattern = '^[0-9+]{10,10}$';
          break;
        case 'number':
          $scope.value = parseInt($scope.value);
          break;
        default:
          break;
      }

      $scope.change = function(){
        $scope.error = undefined;
      }
    }],
  }
});
