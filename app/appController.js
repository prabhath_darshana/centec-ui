app.controller('appController', [
  '$scope',
  '$rootScope',
  '$state',
  'User',
  'authService',
  'userService',
  'httpService',
  'config',
  function($scope, $rootScope, $state, User, authService, userService, httpService, config){
    $scope.user = userService.getUser();
    if(!$scope.user){
      $scope.user = new User();
      $scope.user.getCurrentUser();
      userService.setUser($scope.user);
    }

    $rootScope.$on('$stateChangeStart', function (event, toState, toStateParams, fromState) {
      if(toState.name != 'signin' && (fromState && fromState.name !='signin')){
        var token = authService.getToken();
        if(!token){
          event.preventDefault();
        }else{
          promise = authService.validateToken();
          promise.then(function(response){
            return;
          }).catch(function(response){
            event.preventDefault();
            authService.redirectToSignin();
          });
        }
      }
    });
  }
]);
