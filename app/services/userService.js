app.factory('userService', function(){
  var user = undefined;
  return{
    setUser: function(user){
      this.user = user;
    },
    getUser: function(){
      return this.user;
    }
  }
});
