function httpInterceptor($q, store, config, userService) {
  return {
    request: function(req) {
      if(store.get(config.X_TOKEN)!=undefined){
        req.headers[config.X_TOKEN] = store.get(config.X_TOKEN);
      }
      if(userService.getUser()){
        req.headers[config.USER] = userService.getUser().id;
      }
      return req;
    },

    requestError: function(req) {
      return req;
    },

    response: function(res) {
      return res;
    },

    responseError: function(res) {
      return $q.reject(res);
    }
  }
}

app.factory('httpInterceptor', httpInterceptor)
.config(function($httpProvider) {
  $httpProvider.interceptors.push('httpInterceptor');
})
