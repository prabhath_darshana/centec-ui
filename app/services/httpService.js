app.service('httpService', ['$http', '$q', '$state', 'Upload', 'alertService', 'config',
function($http, $q, $state, Upload, alertService, config){

  /**
  * HTTP GET call
  * @param url
  * @param param
  * @return Promise
  */
  this.get = function(url, param){
    var deferred = $q.defer();

    $http({
      method: 'GET',
      url: config.SERVICE_URL + url,
      headers:{'Content-Type':'application/json'},
      params: param
    }).then(function(response){
      deferred.resolve(response.data);
    }, function(response){
      if(response.status == 401){
        alertService.setAlert('danger', config.AUTH_ERROR, 'signin');
        $state.go('signin');
      }else if(response.status == 500){
        alertService.setAlert('danger', config.SYSTEM_FAILUER, 'signin');
      }else{
        alertService.setAlert('danger', config.SYSTEM_FAILUER_UNKNOWN, 'signin');
      }
      deferred.reject(response);
    });

    return deferred.promise;
  };

  /**
  * HTTP POST call
  * @param url
  * @param param
  * @return Promise
  */
  this.post = function(url, data, param){
    var deferred = $q.defer();

    $http({
      method: 'POST',
      url: config.SERVICE_URL + url,
      headers:{'Content-Type':'application/json'},
      data:data,
      params: param
    }).then(function(response){
      deferred.resolve(response.data);
    }, function(response){
      if(response.status == 401){
        alertService.setAlert('danger', config.AUTH_ERROR, 'signin');
        $state.go('signin');
      }else if(response.status == 500){
        alertService.setAlert('danger', config.SYSTEM_FAILUER, 'signin');
      }else{
        alertService.setAlert('danger', config.SYSTEM_FAILUER_UNKNOWN, 'signin');
      }
      deferred.reject(response);
    });

    return deferred.promise;
  };

  /**
  * HTTP PUT call
  * @param url
  * @param data
  * @param params
  **/
  this.put = function(url, data, params){
    var deferred = $q.defer();

    $http({
      method: 'PUT',
      url: config.SERVICE_URL + url,
      headers:{'Content-Type':'application/json'},
      data:data,
      params: params
    }).then(function(response){
      deferred.resolve(response.data);
    }, function(response){
      if(response.status == 401){
        alertService.setAlert('danger', config.AUTH_ERROR, 'signin');
        $state.go('signin');
      }else if(response.status == 500){
        alertService.setAlert('danger', config.SYSTEM_FAILUER);
      }else{
        alertService.setAlert('danger', config.SYSTEM_FAILUER_UNKNOWN);
      }
      deferred.reject(response);
    });
    return deferred.promise;
  };

  /**
  * HTTP File Upload
  * @param url
  * @param data
  * @param params
  */
  this.upload = function(url, data, params){
    var deferred = $q.defer();

    Upload.upload({
      url: config.SERVICE_URL + url,
      data: data
    }).then(function(response){
      deferred.resolve(response.data);
    }, function(response){
      console.error(response);
      if(response.status == 401){
        alertService.setAlert('danger', config.AUTH_ERROR, 'signin');
        $state.go('signin');
      }else if(response.status == 500){
        alertService.setAlert('danger', config.SYSTEM_FAILUER);
      }else{
        alertService.setAlert('danger', config.SYSTEM_FAILUER_UNKNOWN);
      }
      deferred.reject(response);
    });
    return deferred.promise;
  };

  /**
  * HTTP DELETE call
  * @param url
  * @param param
  * @return Promise
  */
  this.delete = function(url, data, params){
    var deferred = $q.defer();

    $http({
      method: 'DELETE',
      url: config.SERVICE_URL + url,
      headers:{'Content-Type':'application/json'},
      data:data,
      params: params
    }).then(function(response){
      deferred.resolve(response.data);
    }, function(response){
      if(response.status == 401){
        alertService.setAlert('danger', config.AUTH_ERROR, 'signin');
        $state.go('signin');
      }else if(response.status == 500){
        alertService.setAlert('danger', config.SYSTEM_FAILUER);
      }else{
        alertService.setAlert('danger', config.SYSTEM_FAILUER_UNKNOWN);
      }
      deferred.reject(response);
    });
    return deferred.promise;
  }
}]);
