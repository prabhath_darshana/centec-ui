app.service('authService', [
  '$state',
  '$q',
  'store',
  'httpService',
  'config',
  function($state, $q, store, httpService, config){

  this.setToken = function(token){
    store.set(config.X_TOKEN, token);
  }

  this.getToken = function(){
    var token = store.get(config.X_TOKEN);
    if(!token){
      this.redirectToSignin();
    }else{
      return token;
    }
  }

  this.validateToken = function(){
    return $q(function(resolve, reject){
      httpService.get('auth/check')
      .then(function success(data){
        if(data && data.status == config.STATUS.SUCCESS){
          resolve(true);
        }else{
          reject(false);
        }
      }).catch(function(response){
        reject(response);
      });
    });
  }

  this.removeToken = function(){
    store.remove(config.X_TOKEN);
  }

  this.redirectToSignin = function(){
    $state.go('signin');
  }

  this.redirectToDashboard = function(){
    $state.go('home.dashboard');
  }

  this.redirectToLockScreen = function(){
    // $state.go('lock');
  }

}]);
