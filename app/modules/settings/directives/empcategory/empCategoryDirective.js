app.directive('empCategory', function(){
  return {
    restrict:'E',
    templateUrl:'modules/settings/directives/empcategory/empCategory.html',
    controller:[
      '$scope',
      'EmpCategory',
      'EmpCategories',
      'httpService',
      'alertService',
      'config',
      function($scope, EmpCategory, EmpCategories, httpService, alertService, config){
        $scope.init = function(){
          $scope.empCategory = new EmpCategory();
          $scope.empCategories = new EmpCategories();
          $scope.empCategories.getAll();
        }
        $scope.init();

        $scope.addCategory = function(){
          $scope.loading = true;
          $scope.empCategories.errors = undefined;
          $scope.empCategory.create($scope.emp_category)
          .then(function(result){
            $scope.loading = false;
            if(result){
              $scope.init();
            }
          }).catch(function(response){
            $scope.loading = false;
          })
        }

        $scope.deleteEmpCategory = function(empCategory){
          $scope.loading = true;
          empCategory.delete()
          .then(function(result){
            $scope.loading = false;
            if(result){
              $scope.init();
            }
          }).catch(function(response){
            $scope.loading = false;
          })
        }
      }
    ],
  }
});
