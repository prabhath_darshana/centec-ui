app.directive('units', function(){
  return {
    restrict:'E',
    templateUrl:'modules/settings/directives/units/units.html',
    controller:[
      '$scope',
      'Unit',
      'Units',
      'httpService',
      'alertService',
      'config',
      function($scope, Unit, Units, httpService, alertService, config){
        $scope.init = function(){
          $scope.unit = new Unit();
          $scope.units = new Units();
          $scope.units.getAll();
        }
        $scope.init();

        $scope.create = function(){
          $scope.loadingUnits = true;
          $scope.unit.errors = undefined;
          $scope.unit.create()
          .then(function(result){
            $scope.loadingUnits = false;
            if(result){
              $scope.init();
            }
          }).catch(function(response){
            $scope.loadingUnits = false;
          })
        }

        $scope.delete = function(unit){
          $scope.loadingUnits = true;
          unit.delete()
          .then(function(result){
            $scope.loadingUnits = false;
            if(result){
              $scope.init();
            }
          }).catch(function(response){
            $scope.loadingUnits = false;
          })
        }
      }
    ],
  }
});
