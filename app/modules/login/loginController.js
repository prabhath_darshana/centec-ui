app.controller('loginController',
  ['$scope',
  '$rootScope',
  '$http',
  '$state',
  'store',
  'authService',
  'User',
  'alertService',
  'httpService',
  'userService',
  'config',
  function($scope, $rootScope, $http, $state, store, authService, User,
    alertService, httpService, userService, config){

    $scope.validateEmail = function(elem){
      $scope.emailError = config.ERROR_MSG.FIELD_REQ_EMAIL;
      return elem.$touched && !elem.$valid;
    }

    $scope.validatePassword = function(elem){
      $scope.passwordError = config.ERROR_MSG.FIELD_REQ_PASSWORD;
      return elem.$touched && !elem.$valid;
    }

    $scope.btnLogin = function(){
      $scope.user = new User($scope.login);
      $scope.user.signin();
    }
}]);
