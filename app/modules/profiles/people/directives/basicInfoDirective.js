app.directive('basicInfo', function(){
  return{
    restrict:'E',
    templateUrl:'modules/profiles/people/directives/basicInfo.html',
    controller:['$scope','PeopleProfile', 'userService', 'alertService',
    function($scope, PeopleProfile, userService, alertService){
      $scope.user = userService.getUser();
      $scope.peopleProfile = new PeopleProfile();
      $scope.peopleProfile.get($scope.user.profileId);

      $scope.submitBasicInfo = function(){
        console.log($scope.peopleProfile);
        $scope.peopleProfile.update();
      }
    }],
  }
});
