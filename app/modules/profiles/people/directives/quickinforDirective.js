app.directive('quickInfo', function(){
  return{
    restrict: 'E',
    templateUrl: 'modules/profiles/people/directives/quickinfo.html',
    scope:{
      profile:'=',
      designation:'=',
      noteTitle:'@',
      noteBody:'='
    },
    controller: ['$scope', function($scope){
      // $scope.profileId = 1;
    }],
  }
})
