app.directive('clientDashboard', function(){
  return{
    restrict:'E',
    templateUrl:'modules/clients/directives/clientDashboard.html',
    controller:['$scope', '$uibModal', 'Client', 'PeopleProfile', 'alertService',
    function($scope, $uibModal, Client, PeopleProfile, alertService){
      $scope.init = function(){
        $scope.client = new Client();
        $scope.client.get($scope.clientId);
        $scope.client.getPeople($scope.clientId);
      }
      $scope.init();

      var modalInstance = {
        animation:true,
        ariaLabeledBy:'model-title',
        arieaDescribedBy:'model-body',
        templateUrl:'modules/profiles/people/personModal.html',
        size:'lg',
        scope:$scope,
        controller:['$scope','$uibModalInstance', function($scope, $uibModalInstance){
          if(!$scope.peopleProfile){
            $scope.peopleProfile = new PeopleProfile();
          }
          $scope.create = function(){
            $scope.peopleProfile.errors=undefined;
            $scope.loadingViewClientPeople = true;
            $scope.peopleProfile.create('client', $scope.clientId, 'viewClientPeople')
            .then(function(status){
              $scope.loadingViewClientPeople = false;
              if(status){
                $scope.client.setPerson($scope.peopleProfile);
                $uibModalInstance.close();
              }
            }).catch(function(response){
              $scope.loadingViewClientPeople = false;
              $uibModalInstance.close();
            });
          }

          $scope.update = function(){
            $scope.peopleProfile.errors=undefined;
            $scope.loadingViewClientPeople = true;
            $scope.peopleProfile.update('client', $scope.clientId, 'viewClientPeople')
            .then(function(status){
              $scope.loadingViewClientPeople = false;
              if(status){
                $uibModalInstance.close();
              }
            }).catch(function(response){
              $scope.loadingViewClientPeople = false;
              $uibModalInstance.close();
            });
          }

          $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
          }
        }]
      };

      $scope.addPerson = function(){
        $scope.isCreate = true;
        $scope.peopleProfile = undefined;
        $uibModal.open(modalInstance);
      }

      $scope.updatePerson = function(person){
        $scope.isCreate = false;
        $scope.peopleProfile = person;
        $uibModal.open(modalInstance)
      }

      var deletePersonConfirmModal = {
        animation:true,
        ariaLabeledBy:'model-title',
        arieaDescribedBy:'model-body',
        templateUrl:'modules/common/alert/confirmBox.html',
        size:'sm',
        scope:$scope,
        controller:['$scope','$uibModalInstance', function($scope, $uibModalInstance){
          $scope.yes = function(){
            $scope.loadingViewSupplierPeople = true;
            $scope.peopleProfile.delete('client', $scope.clientId, 'viewClientPeople')
            .then(function(result){
              $scope.loadingViewSupplierPeople = false;
              if(result){
                $scope.init();
              }
              $uibModalInstance.close();
            }).catch(function(response){
              $scope.loadingViewSupplierPeople = false;
              $uibModalInstance.close();
            });
          }

          $scope.cancel = function(){
            $uibModalInstance.dismiss();
          }
        }]
      };

      $scope.deletePerson = function(person){
        $scope.peopleProfile = person;
        $scope.confirmbox = {};
        $scope.confirmbox.item = "Client person";
        $scope.confirmbox.description = person.first_name + ' ' + person.last_name;
        $uibModal.open(deletePersonConfirmModal);
      }
    }]
  }
});
