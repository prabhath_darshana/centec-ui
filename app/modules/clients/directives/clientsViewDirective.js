app.directive('clientsView', function(){
  return {
    restrict: 'E',
    templateUrl:'modules/clients/directives/clientsView.html',
    controller:['$scope', '$uibModal', 'Clients', 'httpService', 'config',
    function($scope, $uibModal, Clients, httpService, config){
      $scope.URL_APPENDER = config.URL_APPENDER;
      $scope.init = function(){
        $scope.clients = new Clients();
        $scope.clients.getAll();
      }
      $scope.init();

      $scope.isCreate = true;
      var createClientModalInstance = {
        animation:true,
        ariaLabeledBy:'model-title',
        arieaDescribedBy:'model-body',
        templateUrl:'modules/clients/directives/createClientModal.html',
        size:'lg',
        scope:$scope,
        controller:['$scope','$uibModalInstance', 'Client', function($scope, $uibModalInstance, Client){
          $scope.cancel = function(){
            $uibModalInstance.close();
          }

          $scope.client = $scope.$parent.client;
          $scope.init = function(){
            $scope.client = new Client();
          }
          if(!$scope.client){
            $scope.init();
          }

          $scope.create = function(){
            $scope.client.errors=undefined;
            $scope.loadingClient = true;
            $scope.client.create()
            .then(function(result){
              $scope.loadingClient = false;
              if(result){
                $scope.$parent.clients.setClient($scope.client);
                $scope.init();
                $uibModalInstance.close();
              }
            }).catch(function(){
              $scope.loadingClient = false;
            });
          }
        }]
      };

      var viewPeopleModalInstance = {
        animation:true,
        ariaLabeledBy:'model-title',
        arieaDescribedBy:'model-body',
        templateUrl:'modules/clients/directives/viewPeopleModal.html',
        size:'lg',
        scope:$scope,
        controller:['$scope','$uibModalInstance', function($scope, $uibModalInstance){
          $scope.ok = function(){
            $uibModalInstance.close();
          }
        }]
      };

      var deleteConfirmModalInstance = {
        animation:true,
        ariaLabeledBy:'model-title',
        arieaDescribedBy:'model-body',
        templateUrl:'modules/clients/directives/confirmBox.html',
        size:'sm',
        scope:$scope,
        controller:['$scope','$uibModalInstance', function($scope, $uibModalInstance){
          $scope.yes = function(){
            $scope.loading = true;
            $scope.client.delete()
            .then(function(result){
              $scope.loading = false;
              $scope.init();
              $uibModalInstance.close();
            }).catch(function(response){
              $scope.loading = false;
              $uibModalInstance.dismiss();
            });
          }

          $scope.cancel = function(){
            $uibModalInstance.dismiss();
          }
        }]
      };

      $scope.createClient = function(){
        $uibModal.open(createClientModalInstance);
      }

      $scope.viewPeople = function(){
        $scope.clientName="Abra Kadabra";
        $uibModal.open(viewPeopleModalInstance);
      }

      $scope.delete = function(client){
        console.log(client);
        $scope.client = client;
        $uibModal.open(deleteConfirmModalInstance);
      }
    }],
  }
})
