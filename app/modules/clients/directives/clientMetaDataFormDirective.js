app.directive('clientMetaDataForm', function(){
  return{
    restrict:'E',
    templateUrl:'modules/clients/directives/clientMetaDataForm.html',
    scope:{type:'@', data:"="},
    controller:['$scope', 'Client', function($scope, Client){
      $scope.client = $scope.$parent.client;
      $scope.init = function(){
        $scope.client = new Client();
      }
      if(!$scope.client){
        $scope.init();
      }



      // $scope.create = function(){
      //   $scope.client.errors=undefined;
      //   $scope.loadingClient = true;
      //   $scope.client.create()
      //   .then(function(result){
      //     $scope.loadingClient = false;
      //     if(result){
      //       $scope.$parent.clients.setClient($scope.client);
      //       $scope.init();
      //     }
      //   }).catch(function(){
      //     $scope.loadingClient = false;
      //   });
      // }

      $scope.update = function(){
        $scope.client.errors=undefined;
        $scope.loadingClient = true;
        $scope.client.update()
        .then(function(result){
          $scope.loadingClient = false;
          if(result){
            $scope.$parent.clients.setClient($scope.client);
            $scope.init();
          }
        }).catch(function(){
          $scope.loadingClient = false;
        });
      }
    }],
  }
});
