app.service('alertService', ['$rootScope', '$state', function($rootScope, $state){
  var alert = undefined;

  this.setAlert = function(alertType, alertMsg, panel, alertState){
    this.alert = {};
    this.alert.alertType = alertType;
    this.alert.alertMsg = alertMsg;
    this.alert.panel = panel;
    if(!alertState){
      this.alert.state = $state.current.name;
    }else{
      this.alert.state = alertState;
    }

    /**
    * Broadcasting to alert scope
    **/
    $rootScope.$broadcast('alert', {
      alert:this.alert
    });
  }

  this.getAlert = function(){
    return angular.copy(this.alert);
  }

  this.clearAlerts = function(){
    this.alert = undefined;
    /**
    * Broadcasting to alert scope
    **/
    $rootScope.$broadcast('alert', {
      alert:this.alert
    });
  }
}]);
