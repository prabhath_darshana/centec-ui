app.directive('alert', function(){
  return {
    restrict: 'E',
    templateUrl: 'modules/common/alert/alert.html',
    replace: true,
    scope:{panel:"@"},
    controller:['$scope', '$timeout', '$state', 'alertService',
      function($scope, $timeout, $state, alertService){
        $scope.alert = alertService.getAlert();
        $scope.showAlert = false;
        setAlert();

        function setAlert(){
          if($scope.alert!=undefined
            && $state.current.name == $scope.alert.state
            && $scope.panel == $scope.alert.panel
          ){
            $scope.showAlert = true;
            /**
            * Set auto timeout for alerts
            */
            var alertPromise = $timeout(function () {
              $scope.alert = undefined;
              $scope.showAlert = false;
            }, 60000);
          }
        }

        /**
        * Listen for the event of setAlert
        */
        $scope.$on('alert', function(event, data){
          if(data.alert){
            $scope.alert = data.alert;
            setAlert();
          }else{
            $scope.alert = undefined;
            $scope.showAlert = false;
          }
        });
      }]
  }
});
