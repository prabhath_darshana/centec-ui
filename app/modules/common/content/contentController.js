app.controller('contentController',
['$scope',
 '$state',
 '$rootScope',
 'alertService',
 'config',
function($scope, $state, $rootScope, alertService, config){
  $scope.URL_APPENDER = config.URL_APPENDER;
  $scope.pageTitle = $state.current.title;
  $rootScope.currentState = $state.current.name;
  $rootScope.$on('$stateChangeSuccess',
    function(event, toState, toParams, fromState, fromParams){
      $scope.pageTitle = toState.title;
      $rootScope.currentState = toState.name;
      alertService.clearAlerts();
    });
}]);
