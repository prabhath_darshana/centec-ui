app.directive('headerMain', function(){
  return {
    restrict: 'E',
    templateUrl : 'modules/common/header/header.html',
    controller: ['$scope', '$state', 'userService', 'User', 'PeopleProfile',
    function($scope, $state, userService, User, PeopleProfile){
      $scope.signout = function(){
        $scope.user.signout();
      }

      $scope.userProfile = function(){
        $state.go('home.userprofile');
      }
    }],
  }
});
