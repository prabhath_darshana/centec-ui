app.directive('sidebarLeft', function(){
  return {
    restrict: 'E',
    templateUrl: 'modules/common/sidebar/sidebar.html',
    controller:['$scope', 'config', function($scope, config){
      $scope.URL_APPENDER = config.URL_APPENDER;
    }]
  }
});
