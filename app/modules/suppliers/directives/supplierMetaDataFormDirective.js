app.directive('supplierMetaDataForm', function(){
  return{
    restrict:'E',
    templateUrl:'modules/suppliers/directives/supplierMetaDataForm.html',
    scope:{type:'@'},
    controller:['$scope', 'Supplier', function($scope, Supplier){
      $scope.supplier = $scope.$parent.supplier;
      $scope.init = function(){
        $scope.supplier = new Supplier();
      }
      if(!$scope.supplier){
        $scope.init();
      }

      $scope.create = function(){
        $scope.supplier.errors=undefined;
        $scope.loading = true;
        $scope.supplier.create()
        .then(function(result){
          if(result){
            $scope.$parent.suppliers.setSupplier($scope.supplier);
            $scope.init();
          }
          $scope.loading = false;
        }).catch(function(response){
          $scope.loading = false;
        });
      }

      $scope.update = function(){
        $scope.supplier.errors = undefined;
        $scope.loadingUpdateSupplier = true;
        $scope.supplier.update()
        .then(function(result){
          $scope.loadingUpdateSupplier = false;
        }).catch(function(response){
          $scope.loadingUpdateSupplier = false;
        })
      }
    }],
  }
})
