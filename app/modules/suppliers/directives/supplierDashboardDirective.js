app.directive('supplierDashboard', function(){
  return{
    restrict:'E',
    templateUrl:'modules/suppliers/directives/supplierDashboard.html',
    controller:['$scope', '$uibModal', 'Supplier', 'PeopleProfile', 'alertService',
    function($scope, $uibModal, Supplier, PeopleProfile, alertService){
      $scope.init = function(){
        $scope.supplier = new Supplier();
        $scope.supplier.get($scope.supplierId);
        $scope.supplier.getPeople($scope.supplierId);
      }
      $scope.init();

      var personModal = {
        animation:true,
        ariaLabeledBy:'model-title',
        arieaDescribedBy:'model-body',
        templateUrl:'modules/profiles/people/personModal.html',
        size:'lg',
        scope:$scope,
        controller:['$scope','$uibModalInstance', function($scope, $uibModalInstance){
          if(!$scope.peopleProfile){
            $scope.peopleProfile = new PeopleProfile();
          }

          $scope.create = function(){
            $scope.peopleProfile.errors = undefined;
            $scope.loadingViewSupplierPeople = true;
            $scope.peopleProfile.create('supplier', $scope.supplierId, 'viewSupplierPeople')
            .then(function(result){
              if(result){
                $scope.supplier.setPerson($scope.peopleProfile);
                $scope.loadingViewSupplierPeople = false;
                $uibModalInstance.close();
              }
            }).catch(function(response){
              $scope.loadingViewSupplierPeople = false;
            });
          }

          $scope.update = function(){
            $scope.peopleProfile.errors = undefined;
            $scope.loadingViewSupplierPeople = true;
            $scope.peopleProfile.update('supplier', $scope.supplierId, 'viewSupplierPeople')
            .then(function(result){
              if(result){
                $scope.loadingViewSupplierPeople = false;
                $uibModalInstance.close();
              }
            }).catch(function(response){
              $scope.loadingViewSupplierPeople = false;
            });
          }

          $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
          }
        }]
      };

      $scope.addPerson = function(){
        $scope.isCreate = true;
        $uibModal.open(personModal);
      }

      $scope.updatePerson = function(person){
        $scope.isCreate = false;
        $scope.peopleProfile = person;
        $uibModal.open(personModal);
      }

      var deletePersonConfirmModal = {
        animation:true,
        ariaLabeledBy:'model-title',
        arieaDescribedBy:'model-body',
        templateUrl:'modules/common/alert/confirmBox.html',
        size:'sm',
        scope:$scope,
        controller:['$scope','$uibModalInstance', function($scope, $uibModalInstance){
          $scope.yes = function(){
            $scope.loadingViewSupplierPeople = true;
            $scope.peopleProfile.delete('supplier', $scope.supplierId, 'viewSupplierPeople')
            .then(function(result){
              $scope.loadingViewSupplierPeople = false;
              if(result){
                $scope.init();
              }
              $uibModalInstance.close();
            }).catch(function(response){
              $scope.loadingViewSupplierPeople = false;
              $uibModalInstance.close();
            });
          }

          $scope.cancel = function(){
            $uibModalInstance.dismiss();
          }
        }]
      };

      $scope.deletePerson = function(person){
        $scope.peopleProfile = person;
        $scope.confirmbox = {};
        $scope.confirmbox.item = "Supplier person";
        $scope.confirmbox.description = person.first_name + ' ' + person.last_name;
        $uibModal.open(deletePersonConfirmModal);
      }
    }]
  }
});
