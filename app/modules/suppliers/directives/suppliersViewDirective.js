app.directive('suppliersView', function(){
  return{
    restrict: 'E',
    templateUrl: 'modules/suppliers/directives/suppliersView.html',
    controller:['$scope', '$uibModal', 'Suppliers', 'config',
    function($scope, $uibModal, Suppliers, config){
      $scope.URL_APPENDER = config.URL_APPENDER;
      $scope.init = function(){
        $scope.suppliers = new Suppliers();
        $scope.suppliers.getAll();
      }
      $scope.init();

      var viewPeopleModalInstance = {
        animation:true,
        ariaLabeledBy:'model-title',
        arieaDescribedBy:'model-body',
        templateUrl:'modules/suppliers/directives/viewPeopleModal.html',
        size:'lg',
        scope:$scope,
        controller:['$scope','$uibModalInstance', function($scope, $uibModalInstance){
          $scope.ok = function(){
            $uibModalInstance.close();
          }
        }]
      };

      $scope.viewPeopleModalInstance = function(){
        $scope.clientName="Abra Kadabra";
        $uibModal.open(modalInstance);
      }

      var deleteSupplierModalInstance = {
        animation:true,
        ariaLabeledBy:'model-title',
        arieaDescribedBy:'model-body',
        templateUrl:'modules/common/alert/confirmBox.html',
        size:'sm',
        scope:$scope,
        controller:['$scope','$uibModalInstance', function($scope, $uibModalInstance){
          $scope.yes = function(){
            $scope.loadingSupplierView = true;
            $scope.supplier.delete()
            .then(function(result){
              $scope.loadingSupplierView = false;
              if(result){
                $scope.init();
              }
              $uibModalInstance.close();
            }).catch(function(response){
              $scope.loadingSupplierView = false;
              $uibModalInstance.close();
            });
          }

          $scope.cancel = function(){
            $uibModalInstance.dismiss();
          }
        }]
      };

      $scope.delete = function(supplier){
        $scope.supplier = supplier;
        $scope.confirmbox = {};
        $scope.confirmbox.item = "Supplier";
        $scope.confirmbox.description = supplier.name;
        $uibModal.open(deleteSupplierModalInstance);
      }
    }],
  }
})
