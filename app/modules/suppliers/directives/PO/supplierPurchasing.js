app.directive('supplierPurchasing', function(){
  return {
    restrict:'E',
    templateUrl:'modules/suppliers/directives/PO/supplierPurchasing.html',
    scope:{
      supplier:"="
    },
    controller:['$scope', '$uibModal', function($scope, $uibModal){
      var init = function(){
        
      }

      var newPOModal = {
        animation:true,
        ariaLabeledBy:'model-title',
        arieaDescribedBy:'model-body',
        templateUrl:'modules/suppliers/directives/PO/POModal.html',
        size:'lg',
        scope:$scope,
        controller:'poModalController'
      }

      $scope.newPO = function(){
        $uibModal.open(newPOModal);
      }
    }]
  }
});
