app.controller('deleteWorkModalController',[
  '$scope',
  '$uibModalInstance',
  function($scope, $uibModalInstance){
    $scope.yes = function(){
      $scope.empWork.delete()
      .then(function(response){
        $scope.workLog.load($scope.searchparam);
        $uibModalInstance.close();
      }).catch(function(response){
        $uibModalInstance.close();
      });
    }

    $scope.cancel = function(){
      $uibModalInstance.dismiss();
    }
  }
]);
