app.controller('completePaymentsModalController', [
  '$scope',
  '$uibModalInstance',
  function($scope, $uibModalInstance){
    $scope.cancel = function(){
      $uibModalInstance.close();
    }

    $scope.complete = function(){
      angular.forEach($scope.selectedWorks, function(work){
        work.dopayment()
        .then(function(result){
          $scope.workLog.load($scope.searchparam);
          $uibModalInstance.close();
        }).catch(function(response){
          $uibModalInstance.close();
        });
      });
    }
  }
]);
