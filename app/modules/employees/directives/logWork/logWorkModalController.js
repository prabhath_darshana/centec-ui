app.controller('logWorkModalController', [
  '$scope',
  '$uibModalInstance',
  function($scope, $uibModalInstance){
    $scope.create = function(){
      $scope.empWork.create()
      .then(function(result){
        if(result){
          $scope.workLog.load($scope.searchparam);
          $uibModalInstance.close();
        }
      }).catch(function(response){
        $uibModalInstance.close();
      });
    }

    $scope.update = function(){
      $scope.empWork.update()
      .then(function(result){
        if(result){
          $scope.workLog.load($scope.searchparam);
          $uibModalInstance.close();
        }
      }).catch(function(response){
        $uibModalInstance.close();
      });
    }

    $scope.cancel = function(){
      $uibModalInstance.dismiss('cancel');
    }
  }
])
