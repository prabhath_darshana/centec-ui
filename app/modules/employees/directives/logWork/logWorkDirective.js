app.directive('logWork', function(){
  return {
    restrict:'E',
    templateUrl:'modules/employees/directives/logWork/logWork.html',
    controller:['$scope', '$uibModal', '$filter', 'Projects', 'EmpWork', 'EmpWorkLog', 'config',
    function($scope, $uibModal, $filter, Projects, EmpWork, EmpWorkLog, config){
      $scope.dateFormat = config.APP_DATE_FORMAT;

      $scope.empWork = new EmpWork();
      $scope.empWork.date = new Date();
      $scope.empWork.empId = $scope.employeeId;
      $scope.dateFormat = config.APP_DATE_FORMAT;
      $scope.projects = new Projects();
      $scope.projects.getAll();
      $scope.tblconfig = config.APP_TABLE_CONFIG;

      $scope.workLog = new EmpWorkLog();
      $scope.workLog.works = [];

      $scope.searchparam = {
        empId:$scope.employeeId,
        fromDate: new Date().deductDays(30),
        toDate: new Date(),
        completed:true,
        incomplete:true
      }
      $scope.workLog.load($scope.searchparam);

      $scope.search = function(){
        $scope.searchparam.project = $scope.searchparam.project.id;
        $scope.workLog.load($scope.searchparam);
      }

      var logWorkModal = {
        animation:true,
        ariaLabeledBy:'model-title',
        arieaDescribedBy:'model-body',
        templateUrl:'modules/employees/directives/logWork/logWorkModal.html',
        size:'lg',
        scope:$scope,
        controller:'logWorkModalController'
      }

      $scope.logWork = function(form){
        $scope.isCreate = true;
        $uibModal.open(logWorkModal);
      }

      $scope.updateWork = function(work){
        $scope.empWork=work;
        $scope.isCreate = false;
        $uibModal.open(logWorkModal);
      }

      var deleteWorkModalInstance = {
        animation:true,
        ariaLabeledBy:'model-title',
        arieaDescribedBy:'model-body',
        templateUrl:'modules/common/alert/confirmBox.html',
        size:'sm',
        scope:$scope,
        controller:'deleteWorkModalController'
      };

      $scope.deleteWork = function(work){
        $scope.empWork=work;
        $scope.confirmbox = {};
        $scope.confirmbox.item = "Employee Work";
        var dt = $filter('date')(work.date, $scope.dateFormat);
        $scope.confirmbox.description = dt + " - " + work.work_description;
        $uibModal.open(deleteWorkModalInstance);
      }

      $scope.selectedworks = {
        selectAll:false
      };
      $scope.paymentTotal = 0;
      $scope.selectAll = function(){
        angular.forEach($scope.workLog.works, function(work){
          if(!work.selected){
            work.selected = $scope.selectedworks.selectAll;
            if(!work.selected){
              $scope.paymentTotal -= (work.daily_payable - work.day_paid);
            }else{
              $scope.paymentTotal += (work.daily_payable - work.day_paid);
            }
          }
        });
      }

      var completePaymentsModal = {
        animation:true,
        ariaLabeledBy:'model-title',
        arieaDescribedBy:'model-body',
        templateUrl:'modules/employees/directives/logWork/completePaymentModal.html',
        size:'sm',
        scope:$scope,
        controller:'completePaymentsModalController'
      };

      $scope.completePayments = function(){
        $scope.selectedWorks = $filter('filter')($scope.workLog.works, {selected:true});
        $uibModal.open(completePaymentsModal);
      }

      $scope.selectwork = function(work){
        if(!work.selected){
          $scope.selectedworks.selectAll = false;
          $scope.paymentTotal -= (work.daily_payable - work.day_paid);
        }else{
          $scope.paymentTotal += (work.daily_payable - work.day_paid);
        }
      }
    }],
  }
});
