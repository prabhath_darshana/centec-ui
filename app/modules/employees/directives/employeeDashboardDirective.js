app.directive('employeeDashboard', function(){
  return {
    restrict:'E',
    templateUrl:'modules/employees/directives/employeeDashboard.html',
    controller:['$scope', 'PeopleProfile','Employee', 'config',
    function($scope, PeopleProfile, Employee, config){
      $scope.URL_APPENDER = config.URL_APPENDER;
      $scope.employee = new Employee();
      $scope.employee.get($scope.employeeId);

    }],
  }
})
