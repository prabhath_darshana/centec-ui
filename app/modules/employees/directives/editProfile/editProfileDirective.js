app.directive('editProfile', function(){
  return {
    restrict: 'E',
    templateUrl: 'modules/employees/directives/editProfile/editProfile.html',
    scope:{employee:'='},
    controller:['$scope','EmpCategories', function($scope, EmpCategories){
      $scope.categories = new EmpCategories();
      $scope.categories.getAll();
      console.log($scope.employee);
    }],
  }
});
