app.directive('employeeMetaDataForm', function(){
  return{
    restrict:'E',
    templateUrl:'modules/employees/directives/employeeMetaDataForm.html',
    scope:{type:'@'},
    controller:['$scope', 'Employee','EmpCategories',
     function($scope, Employee, EmpCategories){
      $scope.init = function(){
        if(!$scope.$parent.employee){
          $scope.employee = new Employee();
        }else{
          $scope.employee=$scope.$parent.employee;
        }
        $scope.categories = new EmpCategories();
        $scope.categories.getAll();
      }
      $scope.init ();


      $scope.create = function(){
        console.log($scope.employee);
        $scope.employee.errors = undefined;
        $scope.$parent.loading = true;
        $scope.employee.create()
        .then(function(result){
          $scope.$parent.loading = false;
          if(result){
            $scope.$parent.employees.setEmployee($scope.employee);
            $scope.init();
          }
        }).catch(function(response){
          $scope.$parent.loading = false;
        });
      }
    }],
  }
});
