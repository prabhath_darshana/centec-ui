app.directive('employeesView', function(){
  return {
    restrict:'E',
    templateUrl:'modules/employees/directives/employeesView.html',
    controller:['$scope','$uibModal', '$filter', 'Employees', 'config',
    function($scope, $uibModal, $filter, Employees, config){
      $scope.URL_APPENDER = config.URL_APPENDER;
      $scope.init = function(){
        $scope.employees = new Employees();
        $scope.filteredEmployees = [];
        $scope.employees.getAll()
        .then(function(result){
          $scope.filteredEmployees = $scope.employees.array;
        });
        $scope.tableConfig = config.APP_TABLE_CONFIG;
      }
      $scope.init();

      $scope.searchEmployees = function(){
        $scope.filteredEmployees = $filter('filter')($scope.employees.array, $scope.searchEmployeesQuery);
      }

      var modalInstance = {
        animation:true,
        ariaLabeledBy:'model-title',
        arieaDescribedBy:'model-body',
        templateUrl:'modules/common/alert/confirmBox.html',
        size:'sm',
        scope:$scope,
        controller:['$scope','$uibModalInstance', function($scope, $uibModalInstance){
          $scope.yes = function(){
            $scope.loadingViewEmployees = true;
            $scope.employee.delete()
            .then(function(result){
              $scope.loadingViewEmployees = false;
              $scope.init();
              $uibModalInstance.close();
            }).catch(function(response){
              $scope.loading = false;
            });
          }

          $scope.cancel = function(){
            $uibModalInstance.dismiss();
          }
        }]
      };

      $scope.deleteEmployee = function(employee){
        $scope.employee=employee;
        $scope.confirmbox = {};
        $scope.confirmbox.item = "Employee";
        $scope.confirmbox.description = employee.first_name + ' ' + employee.last_name;
        $uibModal.open(modalInstance);
      }
    }],
  }
});
