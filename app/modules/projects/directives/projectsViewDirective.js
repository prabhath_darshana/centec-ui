app.directive('projectsView', function(){
  return{
    restrict:'E',
    templateUrl:'modules/projects/directives/projectsView.html',
    controller:['$scope', 'Projects', 'Project', 'Clients', 'config', '$uibModal',
    function($scope, Projects, Project, Clients, config, $uibModal){
      $scope.URL_APPENDER = config.URL_APPENDER;
      $scope.init = function(){
        $scope.projects = new Projects();
        $scope.projects.getAll();
        $scope.clients = new Clients();
        $scope.clients.getForSuggest();
        $scope.project = new Project();
        if($scope.type=='update'){
          $scope.project.getDetails($scope.$parent.projectId);
        }
      }
      $scope.init();

      $scope.isCreate = true;
      var createProjectModalInstance = {
        animation:true,
        ariaLabeledBy:'model-title',
        arieaDescribedBy:'model-body',
        templateUrl:'modules/projects/directives/projectModal.html',
        size:'lg',
        scope:$scope,
        controller:['$scope','$uibModalInstance', 'Project',
        function($scope, $uibModalInstance, Project){
          $scope.cancel = function(){
            $uibModalInstance.close();
          }

          $scope.create = function(){
            $scope.project.errors = undefined;
            $scope.project.create()
            .then(function(result){
              if(result){
                $scope.$parent.projects.setProject($scope.project);
                $scope.init();
                $uibModalInstance.close();
              }
            });
          }

          $scope.update = function(){
            $scope.loadingUpdateProject = true;
            $scope.project.errors = undefined;
            $scope.project.update()
            .then(function(result){
              $scope.loadingUpdateProject = false;
            }).catch(function(response){
              $scope.loadingUpdateProject = false;
            });
          }
        }]
      }

      $scope.createProject = function(){
        $uibModal.open(createProjectModalInstance);
      }
    }],
  }
});
