app.directive('projectDashboard', function(){
  return {
    restrict:'E',
    templateUrl:'modules/projects/directives/projectDashboard.html',
    controller:['$scope', '$state', 'Project', 'config', function($scope, $state, Project, config){
      $scope.URL_APPENDER = config.URL_APPENDER;
      $scope.project = new Project();
      $scope.project.getDetails($scope.projectId);
    }],
  }
});
