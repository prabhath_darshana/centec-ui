app.directive('projectMetaDataForm', function(){
  return{
    restrict:'E',
    templateUrl:'modules/projects/directives/projectMetaDataFrom.html',
    scope:{type:'@'},
    controller:['$scope', 'Clients', 'Project', function($scope, Clients, Project){
      // angular.copy($scope.project, $scope.$parent.project);
      // $scope.project = $scope.$parent.project;
      $scope.init = function(){
        $scope.clients = new Clients();
        $scope.clients.getForSuggest();
        $scope.project = new Project();
        if($scope.type=='update'){
          $scope.project.getDetails($scope.$parent.projectId);
        }
      }
      $scope.init();

      $scope.create = function(){
        $scope.project.errors = undefined;
        $scope.project.create()
        .then(function(result){
          if(result){
            $scope.$parent.projects.setProject($scope.project);
            $scope.init();
          }
        });
      }

      $scope.update = function(){
        $scope.loadingUpdateProject = true;
        $scope.project.errors = undefined;
        $scope.project.update()
        .then(function(result){
          $scope.loadingUpdateProject = false;
        }).catch(function(response){
          $scope.loadingUpdateProject = false;
        });
      }
    }]
  }
});
