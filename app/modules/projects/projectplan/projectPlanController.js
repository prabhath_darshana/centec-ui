app.controller('projectPlanController',
['$scope',
 '$uibModal',
 'ProjectPlan',
 'ProjectPlanItem',
 'ProjectTask',
  function($scope, $uibModal, ProjectPlan, ProjectPlanItem, ProjectTask){
    $scope.init = function(){
      $scope.plan = new ProjectPlan();
      $scope.plan.get($scope.projectId);
      $scope.data = [
        {name: 'Parent 1'},
        {name: 'One', parent:'Parent 1', tasks:[
            {name: 'task1', from:'2017-01-01' , to: '2017-01-10'},
            {name: 'task2', from:'2017-01-12' , to: '2017-01-16'}
            ]
        },
        {name: 'row2', tasks: [
            {name: 'task1', from:'2017-01-10' , to: '2017-01-15'},
            {name: 'task2', from:'2017-01-12' , to: '2017-01-16'}
            ]
        },
      ]
    }
    $scope.init();

    var planModalInstance = {
      animation:true,
      ariaLabeledBy:'model-title',
      arieaDescribedBy:'model-body',
      templateUrl:'modules/projects/projectplan/planModal.html',
      size:'sm',
      scope:$scope,
      controller:'planModalController'
    };

    $scope.createPlanItem = function(){
      $scope.isCreate = true;
      $uibModal.open(planModalInstance);
    }

    $scope.editPlanItem = function(planItem){
      $scope.isCreate = false;
      $scope.planItem = planItem;
      $uibModal.open(planModalInstance);
    }

    var deletePlanItemConfirmModal = {
      animation:true,
      ariaLabeledBy:'model-title',
      arieaDescribedBy:'model-body',
      templateUrl:'modules/common/alert/confirmBox.html',
      size:'sm',
      scope:$scope,
      controller:'deletePlanItemController'
    };


    $scope.deletePlanItem = function(planItem){
      $scope.planItem = planItem;
      $scope.confirmbox = {};
      $scope.confirmbox.item = "Project Plan Item";
      $scope.confirmbox.description = planItem.code + ' - ' + planItem.description;
      $uibModal.open(deletePlanItemConfirmModal);
    }

    var taskModalInstance = {
      animation:true,
      ariaLabeledBy:'model-title',
      arieaDescribedBy:'model-body',
      templateUrl:'modules/projects/projectplan/taskModal.html',
      size:'lg',
      scope:$scope,
      controller:'taskModalController'
    };

    $scope.createTask = function(planItem){
      $scope.isCreate = true;
      $scope.planItem = planItem;
      $scope.projectTask = new ProjectTask();
      $uibModal.open(taskModalInstance);
    }

    $scope.updateTask = function(task){
      $scope.isCreate = false;
      $scope.projectTask = task;
      $uibModal.open(taskModalInstance);
    }

    var deleteTaskConfirmModal = {
      animation:true,
      ariaLabeledBy:'model-title',
      arieaDescribedBy:'model-body',
      templateUrl:'modules/common/alert/confirmBox.html',
      size:'sm',
      scope:$scope,
      controller:'deleteTaskController'
    };

    $scope.deleteTask = function(task){
      $scope.projectTask = task;
      $scope.confirmbox = {};
      $scope.confirmbox.item = "Project Task Item";
      $scope.confirmbox.description = task.code + ' - ' + task.description;
      $uibModal.open(deleteTaskConfirmModal);
    }
  }
]);
