app.controller('deletePlanItemController', ['$scope','$uibModalInstance',
function($scope, $uibModalInstance){
  $scope.yes = function(){
    $scope.loadingProjectPlan = true;
    $scope.planItem.delete()
    .then(function(result){
      $scope.loadingProjectPlan = false;
      if(result){
        $scope.init();
      }
      $uibModalInstance.close();
    }).catch(function(response){
      $scope.loadingProjectPlan = false;
      $uibModalInstance.close();
    });
  }

  $scope.cancel = function(){
    $uibModalInstance.dismiss();
  }
}]);
