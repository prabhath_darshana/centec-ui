app.controller('planModalController', ['$scope','$uibModalInstance','ProjectPlanItem',
function($scope, $uibModalInstance, ProjectPlanItem){
  if(!$scope.planItem){
    $scope.planItem = new ProjectPlanItem();
  }
  
  $scope.create = function(){
    $scope.loadingProjectPlan = true;
    $scope.planItem.errors = undefined;
    $scope.planItem.create($scope.projectId)
    .then(function(response){
      if(response){
        $scope.init();
        $uibModalInstance.close();
      }
      $scope.loadingProjectPlan = false;
    }).catch(function(response){
      $scope.loadingProjectPlan = false;
    });
  }

  $scope.update = function(){
    $scope.loadingProjectPlan = true;
    $scope.planItem.errors = undefined;
    $scope.planItem.update($scope.projectId)
    .then(function(response){
      if(response){
        $scope.init();
        $uibModalInstance.close();
      }
      $scope.loadingProjectPlan = false;
    }).catch(function(response){
      $scope.loadingProjectPlan = false;
    });
  }

  $scope.cancel = function(){
    $uibModalInstance.close();
  }
}]);
