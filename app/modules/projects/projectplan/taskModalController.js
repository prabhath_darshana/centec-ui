app.controller('taskModalController', [
  '$scope',
  '$uibModalInstance',
  'Units',
  function($scope, $uibModalInstance, Units){
    $scope.init = function(){
      $scope.units = new Units();
      $scope.units.getAll();

      $scope.projectTask.unit = {
        id:$scope.projectTask.unitId,
        name:$scope.projectTask.unitName
      }
    }
    $scope.init();

    $scope.create = function(){
      $scope.loadingProjectPlan = true;
      $scope.projectTask.errors = undefined;
      $scope.projectTask.create($scope.planItem.id)
      .then(function(result){
        if(result){
          $scope.init();
          $uibModalInstance.close();
        }
        $scope.loadingProjectPlan = false;
      }).catch(function(response){
        $scope.loadingProjectPlan = false;
      });
    }

    $scope.update = function(){
      $scope.loadingProjectPlan = true;
      $scope.projectTask.errors = undefined;
      $scope.projectTask.update()
      .then(function(result){
        if(result){
          $scope.init();
          $uibModalInstance.close();
        }
        $scope.loadingProjectPlan = false;
      }).catch(function(response){
        $scope.loadingProjectPlan = false;
      });
    }

    $scope.cancel = function(){
      $uibModalInstance.close();
    }
  }
]);
