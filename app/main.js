var app = angular.module('centecDashboard',
                              ['config',
                               'ui.router',
                               'angular-storage',
                               'ui.bootstrap',
                               'ngFileUpload',
                               'angucomplete-alt',
                               'angular-table',
                               'angularMoment',
                               'gantt',
                               'gantt.table',
                               'gantt.tree'
                              ]);

app.config(function($stateProvider, $urlRouterProvider, config){
  //Redirect invalid routes to dashboard
  $urlRouterProvider.otherwise('/dashboard');

  $stateProvider
    .state('signin',{
      url: '/signin',
      templateUrl:'modules/login/login.html',
      title: 'Login',
      controller:'loginController'
    })
    .state('home',{
      url: '',
      abstract: true,
      templateUrl:'modules/common/content/content.html',
      controller: 'contentController'
    })
    .state('home.dashboard',{
      url: '/dashboard',
      templateUrl:'modules/dashboard/dashboard.html',
      title: 'Dashboard',
      controller: 'dashboardController'
    })
    .state('home.userprofile',{
      url: '/userprofile',
      templateUrl:'modules/profiles/people/peopleProfile.html',
      title: 'User Profile',
      controller: 'peopleProfileController'
    })
    .state('home.projects',{
      url:'/projects/{projectId}',
      templateUrl:'modules/projects/projects.html',
      title: 'Projects',
      controller: 'projectsController'
    })
    .state('home.projects.projectplan', {
      url:'/plan',
      templateUrl:'modules/projects/projectplan/projectPlan.html',
      title: 'Project Plan',
      controller: 'projectPlanController'
    })
    .state('home.projects.expense', {
      url:'/expense',
      templateUrl:'modules/projects/projectexpense/projectExpense.html',
      title:'Projects Expense',
      controller:'projectExpenseController',
    })
    .state('home.projects.details',{
      url:'/details',
      templateUrl:'modules/projects/projectdetails/projectDetails.html',
      title:'Project Details',
      controller:'projectDetailsController'
    })
    .state('home.clients',{
      url:'/clients/{clientId}',
      templateUrl:'modules/clients/clients.html',
      title: 'Clients',
      controller:'clientsController'
    })
    .state('home.suppliers',{
      url:'/suppliers/{supplierId}',
      templateUrl:'modules/suppliers/sippliers.html',
      title:'Suppliers',
      controller:'suppliersController'
    })
    .state('home.employees', {
      url:'/employees/{employeeId}',
      templateUrl:'modules/employees/employees.html',
      title:'Employees',
      controller:'employeesController'
    })
    .state('home.assets', {
      url:'/assets',
      templateUrl:'modules/assets/assets.html',
      title:'Assets',
      controller:'assestsController'
    })
    .state('home.appsettings', {
      url:'/settings/app',
      templateUrl:'modules/settings/appSettings.html',
      title:'Application Settings',
      controller:'appSettingsController',
    });
})
