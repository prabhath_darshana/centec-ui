app.factory('PeopleProfile', ['$q', 'httpService', 'alertService', 'config',
function($q, httpService, alertService, config){

  function PeopleProfile(peopleProfileData){
    if(peopleProfileData){
      this.setData(peopleProfileData);
    }
  };

  PeopleProfile.prototype = {
    setData:function(userProfileData){
      angular.extend(this, userProfileData);
    },

    get:function(profileId){
      var params = {
        profileId:profileId,
      }

      var scope = this;
      httpService.get('people/profile', params)
      .then(function(data){
        scope.setData(data);
      }).catch(function(response){
        console.error(response);
      });
    },

    create:function(type, id, panel){
      var scope = this;

      var params = {};
      if(type){
        params = {
          type:type,
          id:id
        }
      }

      return $q(function(resolve, reject){
        httpService.post('people/create', scope, params)
        .then(function(response){
          if(response.status === 'success'){
            alertService.setAlert('success', config.SUCCESS_MSG.CREATE_ITEM, panel);
            resolve(true);
          }else{
            alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, panel);
            resolve(false);
          }
          scope.setData(response);
        }).catch(function(response){
          alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, panel);
          console.error(response);
          reject(response);
        });
      });
    },

    update:function(type, id, panel){
      var scope = this;

      var params = {};
      if(type){
        params = {
          type:type,
          id:id
        }
      }

      return $q(function(resolve, reject){
        httpService.put('people/update', scope, params)
        .then(function(response){
          if(response.status === 'success'){
            alertService.setAlert('success', config.SUCCESS_MSG.UPDATE_ITEM, panel);
            resolve(true);
          }else{
            alertService.setAlert('danger', config.ERROR_MSG.UPDATE_ITEM, panel);
            resolve(false);
          }
          scope.setData(response);
        }).catch(function(response){
          alertService.setAlert('danger', config.ERROR_MSG.UPDATE_ITEM, panel);
          console.error(response);
          reject(response);
        });
      });
    },

    delete:function(type, id, panel){
      var scope = this;

      var params = {};
      if(type){
        params = {
          type:type,
          id:id
        }
      }

      return $q(function(resolve, reject){
        httpService.delete('people/delete', scope, params)
        .then(function(response){
          if(response.status === 'success'){
            alertService.setAlert('success', config.SUCCESS_MSG.DELETE_ITEM, panel);
            resolve(true);
          }else{
            alertService.setAlert('danger', config.ERROR_MSG.DELETE_ITEM, panel);
            resolve(false);
          }
        }).catch(function(response){
          alertService.setAlert('danger', config.ERROR_MSG.DELETE_ITEM, panel);
          console.error(response);
          reject(response);
        });
      });
    }
  };
  return PeopleProfile;
}]);
