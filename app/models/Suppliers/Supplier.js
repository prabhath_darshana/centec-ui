app.factory('Supplier', [
  '$q',
  'PeopleProfile',
  'httpService',
  'alertService',
  'config',
  function($q, PeopleProfile, httpService, alertService, config){
    function Supplier(supplierData){
      this.setData(supplierData);
    }

    Supplier.prototype = {
      setData:function(supplierData){
        angular.extend(this, supplierData)
      },

      setPeople:function(people){
        var scope = this;
        scope.people = [];

        if(people){
          people.forEach(function(person){
            scope.people.push(new PeopleProfile(person));
          })
        }
        angular.extend(this,scope);
      },

      setPerson:function(person){
        var scope = this;

        if(person){
          if(!scope.people){
            scope.people = [];
          }
          scope.people.push(new PeopleProfile(person));
        }
      },

      get:function(supplierId){
        var scope = this;

        var params = {
          supplierId:supplierId,
        }

        httpService.get('supplier/get', params)
        .then(function(response){
          scope.setData(response);
        }).catch(function(response){
          console.error(response);
        });
      },

      create:function(){
        var scope = this;

        return $q(function(resolve, reject){
          httpService.post('supplier/create', scope)
          .then(function(response){
            if(response.status == 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.CREATE_ITEM, 'createSupplier');
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, 'createSupplier');
              resolve(false);
            }
            scope.setData(response);
          }).catch(function(response){
            console.error(response);
            alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, 'createSupplier');
            reject(response);
          });
        });
      },

      update:function(){
        var scope = this;

        return $q(function(resolve, reject){
          httpService.put('supplier/update', scope)
          .then(function(response){
            if(response.status == 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.UPDATE_ITEM, 'updateSupplier');
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.UPDATE_ITEM, 'updateSupplier');
              resolve(false);
            }
            scope.setData(response);
          }).catch(function(response){
            console.error(response);
            alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, 'updateSupplier');
            reject(response);
          })
        });
      },

      delete:function(){
        var scope = this;

        return $q(function(resolve, reject){
          httpService.delete('supplier/delete', scope)
          .then(function(response){
            if(response.status == 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.DELETE_ITEM, 'viewSupplier');
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.DELETE_ITEM, 'viewSupplier');
              resolve(false);
            }
          }).catch(function(response){
            console.error(response);
            alertService.setAlert('danger', config.ERROR_MSG.DELETE_ITEM, 'viewSupplier');
            reject(response);
          });
        });
      },

      getPeople:function(supplierId){
        var scope = this;

        var param = {
          type:'supplier',
          id:supplierId
        }

        httpService.get('people/getall', param)
        .then(function(response){
          scope.setPeople(response);
        }).catch(function(response){
          console.error(response);
        });
      }
    }
    return Supplier;
  }
]);
