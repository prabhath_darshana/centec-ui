app.factory('SupplierPO', [
  '$q',
  'httpService',
  'alertService',
  'config',
  function($q, httpService, alertService, config){
    function SupplierPO(supplierPOData){
      if(supplierPOData){
        this.setData(supplierPOData);
      }

      SupplierPO.prototype = {
        setData:function(supplierPOData){
          var date = new Date(supplierPOData.date);
          var po = {
            id:supplierPOData.id,
            quantity:supplierPOData.quantity,
            unit:supplierPOData.unit,
            rate:supplierPOData.rate,
            payment:supplierPOData.payment,
            paymentType:supplierPOData.paymentType,
            note:supplierPOData.note,
            project:supplierPOData.project,
            date:date,
          }
          angular.extend(this, po);
        },

        create:function(){
          var scope = this;

          return $q(function(resolve, reject){
            httpService.post('supplier/po/create', scope)
            .then(function(response){
              console.log(response);
              if(response.status == 'success'){
                alertService.setAlert('success', config.SUCCESS_MSG.CREATE_ITEM, 'createSupplier');
                resolve(true);
              }else{
                alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, 'createSupplier');
                resolve(false);
              }
              scope.setData(response);
            }).catch(function(response){
              console.error(response);
              alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, 'createSupplier');
              reject(response);
            });
          });
        }
      }
    }
    return SupplierPO;
  }
])
