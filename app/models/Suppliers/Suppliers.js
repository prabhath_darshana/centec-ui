app.factory('Suppliers', [
  'Supplier',
  'httpService',
  'config',
  function(Supplier, httpService, config){
    function Suppliers(suppliersData){
      this.setData(suppliersData);
    }

    Suppliers.prototype = {
      setData:function(suppliersData){
        var array = [];

        if(suppliersData){
          suppliersData.forEach(function(supplierData){
            array.push(new Supplier(supplierData));
          });
        }

        var suppliers = {
          array:array
        }

        angular.extend(this, suppliers);
      },

      setSupplier:function(supplierData){
        var scope = this;
        if(supplierData instanceof Supplier){
          scope.array.push(supplierData);
        }
      },

      getAll:function(){
        var scope = this;

        httpService.get('suppliers/getall')
        .then(function(response){
          scope.setData(response);
        }).catch(function(response){
          console.error(response);
        });
      }
    }
    return Suppliers;
  }
])
