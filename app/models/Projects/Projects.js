app.factory('Projects', [
  'Project',
  'httpService',
  'config',
  function(Project, httpService, config){

    function Projects(projectsData){
      this.setData(projectsData);
    }

    Projects.prototype = {
      setData:function(projectsData){
        var array = [];

        if(projectsData){
          projectsData.forEach(function(projectData){
            array.push(new Project(projectData));
          });
        }

        var projects = {
          array:array
        };

        angular.extend(this, projects);
      },

      /*
      * Set Project to the Project array in Projects list
      * this method will call after creating new Project.
      * @param Project
      */
      setProject:function(projectData){
        var scope = this;
        if(projectData instanceof Project){
          scope.array.push(projectData);
        }
      },

      getAll:function(){
        var scope = this;

        httpService.get('projects/getall')
        .then(function(response){
          scope.setData(response);
        }).catch(function(response){
          console.error(response);
        });
      }

    }
    return Projects;
  }
]);
