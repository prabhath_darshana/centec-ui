app.factory('ProjectPlan', [
  'ProjectPlanItem',
  'httpService',
  'config',
  function(ProjectPlanItem, httpService, config){
    function ProjectPlan(projectPlanData){
      this.setData(ProjectPlan);
    }

    ProjectPlan.prototype = {
      setData:function(projectPlanData){
        var planItems = [];
        
        if(projectPlanData.planItems){
          projectPlanData.planItems.forEach(function(planItem){
            planItems.push(new ProjectPlanItem(planItem));
          });
        }

        var projectPlan = {
          planItems:planItems
        }

        angular.extend(this, projectPlan);
      },

      get:function(projectId){
        var scope = this;

        var params = {
          projectId:projectId
        }

        httpService.get('project/plan/get', params)
        .then(function(response){
          scope.setData(response);
        }).catch(function(response){
          console.error(response);
        });
      }
    }
    return ProjectPlan;
  }
])
