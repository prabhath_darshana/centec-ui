app.factory('ProjectTask', [
  '$q',
  'httpService',
  'alertService',
  'config',
  function($q, httpService, alertService, config){
    function ProjectTask(projectTaskData){
      this.setData(projectTaskData);
    }

    ProjectTask.prototype = {
      setData:function(projectTaskData){
        angular.extend(this, projectTaskData);
      },

      create:function(planId){
        var scope = this;

        var params = {
          planId:planId
        }

        return $q(function(resolve, reject){
          httpService.post('project/task/create', scope, params)
          .then(function(response){
            if(response.status == 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.CREATE_ITEM, "projectPlan");
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, "projectPlan");
              resolve(false);
            }
            scope.setData(response);
          }).catch(function(response){
            console.error(response);
            alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, "projectPlan");
            reject(response);
          });
        });
      },

      update:function(){
        var scope = this;

        return $q(function(resolve, reject){
          httpService.put('project/task/update', scope)
          .then(function(response){
            if(response.status == 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.UPDATE_ITEM, "projectPlan");
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.UPDATE_ITEM, "projectPlan");
              resolve(false);
            }
            scope.setData(response);
          }).catch(function(response){
            console.error(response);
            alertService.setAlert('danger', config.ERROR_MSG.UPDATE_ITEM, "projectPlan");
            reject(response);
          });
        });
      },

      delete:function(){
        var scope = this;

        return $q(function(resolve, reject){
          httpService.delete('project/task/delete', scope)
          .then(function(response){
            if(response.status == 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.DELETE_ITEM, "projectPlan");
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.DELETE_ITEM, "projectPlan");
              resolve(false);
            }
          }).catch(function(response){
            console.error(response);
            alertService.setAlert('danger', config.ERROR_MSG.DELETE_ITEM, "projectPlan");
            reject(response);
          });
        });
      }

    }
    return ProjectTask;
  }
])
