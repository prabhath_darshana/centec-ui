app.factory('Project', [
  '$q',
  'httpService',
  'alertService',
  'config',
  function($q, httpService, alertService, config){
    function Project(projectData){
      if(projectData){
        this.setData(projectData);
      }
    }

    Project.prototype = {
      setData:function(projectData){
        angular.extend(this, projectData);
      },

      getDetails:function(projectId){
        var scope = this;

        var params = {
          projectId:projectId
        }

        httpService.get('project/get', params)
        .then(function(response){
          scope.setData(response);
        }).catch(function(response){
          console.error(response);
        });
      },

      create:function(){
        var scope = this;
        return $q(function(resolve, reject){
          httpService.post('project/create', scope)
          .then(function(response){
            if(response.status == 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.CREATE_ITEM, "createProject");
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, "createProject");
              resolve(false);
            }
            scope.setData(response);
          }).catch(function(response){
            console.error(response);
            alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, "createProject");
            reject(response);
          });
        });
      },

      update:function(){
        var scope = this;
        return $q(function(resolve, reject){
          httpService.put('project/update', scope)
          .then(function(response){
            if(response.status == 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.UPDATE_ITEM, "updateProject");
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.UPDATE_ITEM, "updateProject");
              resolve(false);
            }
            scope.setData(response);
          }).catch(function(response){
            console.error(response);
            alertService.setAlert('danger', config.ERROR_MSG.UPDATE_ITEM, "updateProject");
          });
        });
      }
    }

    return Project;
  }
])
