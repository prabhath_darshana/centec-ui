app.factory('ProjectPlanItem', [
  '$q',
  'httpService',
  'alertService',
  'config',
  'ProjectTask',
  function($q, httpService, alertService, config, ProjectTask){

    function ProjectPlanItem(projectPlanItemData){
      if(projectPlanItemData){
        this.setData(projectPlanItemData);
      }
    }

    ProjectPlanItem.prototype = {
      setData:function(projectPlanItemData){
        var tasks = [];

        if(projectPlanItemData.tasks){
          projectPlanItemData.tasks.forEach(function(projectTask){
            tasks.push(new ProjectTask(projectTask));
          });
        }

        var projectPlanItem = {
          id:projectPlanItemData.id,
          code:projectPlanItemData.code,
          description:projectPlanItemData.description,
          tasks:tasks
        }
        angular.extend(this, projectPlanItem);
      },

      get:function(planId){
        var scope = this;

        var params = {
          planId:planId
        }

        httpService.get('project/plan/item/get', params)
        .then(function(response){
          this.setData(response);
        }).catch(function(response){
          console.error(response);
        });
      },

      create:function(projectId){
        var scope = this;

        var params = {
          projectId:projectId
        }

        return $q(function(resolve, reject){
          httpService.post('project/plan/item/create', scope, params)
          .then(function(response){
            if(response.status == 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.CREATE_ITEM, "projectPlan");
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, "projectPlan");
              resolve(false);
            }
            scope.setData(response);
          }).catch(function(response){
            console.error(response);
            alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, "projectPlan");
            reject(response);
          });
        });
      },

      update:function(){
        var scope = this;

        return $q(function(resolve, reject){
          httpService.put('project/plan/item/update', scope)
          .then(function(response){
            if(response.status == 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.CREATE_ITEM, "projectPlan");
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, "projectPlan");
              resolve(false);
            }
            scope.setData(response);
          }).catch(function(response){
            console.error(response);
            alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, "projectPlan");
            reject(response);
          });
        });
      },

      delete:function(){
        var scope = this;

        return $q(function(resolve, reject){
          httpService.delete('project/plan/item/delete', scope)
          .then(function(response){
            if(response.status == 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.DELETE_ITEM, "projectPlan");
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.DELETE_ITEM, "projectPlan");
              resolve(false);
            }
          }).catch(function(response){
            console.error(response);
            alertService.setAlert('danger', config.ERROR_MSG.DELETE_ITEM, "projectPlan");
            reject(response);
          });
        });
      }
    }
    return ProjectPlanItem;
  }
])
