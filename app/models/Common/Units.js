app.factory('Units', [
  'Unit',
  'httpService',
  'config',
  function(Unit, httpService, config){
    function Units(unitsData){
      this.setData(unitsData);
    }

    Units.prototype = {
      setData:function(unitsData){
        var units = [];

        if(unitsData){
          unitsData.forEach(function(unitData){
            units.push(new Unit(unitData));
          });
        }

        angular.extend(this, units);
      },

      getAll:function(){
        var scope = this;

        httpService.get('unit/getall')
        .then(function(response){
          scope.setData(response);
        }).catch(function(response){
          console.error(response);
        });
      }
    }
    return Units;
  }
])
