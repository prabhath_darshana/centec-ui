app.factory('Unit',[
  '$q',
  'httpService',
  'alertService',
  'config',
  function($q, httpService, alertService, config){

    function Unit(unitData){
      this.setData(unitData);
    }

    Unit.prototype = {
      setData:function(unitData){
        angular.extend(this, unitData);
      },

      create:function(){
        var scope = this;

        return $q(function(resolve, reject){
          httpService.post('unit/create', scope)
          .then(function(response){
            if(response.status == 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.CREATE_ITEM, 'unit');
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, 'unit');
              resolve(false);
            }
            scope.setData(response);
          }).catch(function(response){
            alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, 'unit');
            console.error(response);
            reject(response);
          });
        });
      },

      delete:function(){
        var scope = this;

        return $q(function(resolve, reject){
          httpService.delete('unit/delete', scope)
          .then(function(response){
            if(response.status == 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.DELETE_ITEM, 'unit');
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.DELETE_ITEM, 'unit');
              resolve(false);
            }
            scope.setData(response);
          }).catch(function(response){
            alertService.setAlert('danger', config.ERROR_MSG.DELETE_ITEM, 'unit');
            console.error(response);
            reject(response);
          });
        });
      }
    }
    return Unit;
  }
])
