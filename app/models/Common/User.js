app.factory('User', ['httpService', 'alertService', 'authService', 'userService', 'config',
function(httpService, alertService, authService, userService, config){

  function User(userData){
    if(userData){
      this.setData(userData);
    }
  }

  User.prototype = {
    setData: function(userData){
      angular.extend(this, userData);
    },

    signin: function(){
      var data = {
        email: this.email,
        password: this.password
      };

      var scope = this;

      httpService.post('auth/signin', data)
      .then(function(response){
        if(response.status === config.STATUS.SUCCESS){
          authService.setToken(response.X_TOKEN);
          authService.redirectToDashboard();
          scope.setData(response.user);
          userService.setUser(scope);
        }else{
          alertService.setAlert('danger', 'Incorrect email or password entered. Please try again.');
        }
      }).catch(function(response){
        console.error("error signin");
        //Default errors will be handled. custom codes goes here
      });
    },

    signout: function(){
      if(authService.getToken()){
        authService.removeToken();
        authService.redirectToSignin();
      }
    },

    getCurrentUser: function(){
      var param = {
        token: authService.getToken()
      };

      var scope = this;

      httpService.get('auth/user', param)
      .then(function(data){
        scope.setData(data);
      }).catch(function(response){
        console.error(response);
        authService.redirectToSignin();
      });
    },

    create: function(){

    }
  };

  return User;
}]);
