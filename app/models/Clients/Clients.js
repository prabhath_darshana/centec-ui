app.factory('Clients', [
  'Client',
  'httpService',
  function(Client, httpService){
    function Clients(clientsData){
      if(clientsData){
        this.setData(clientsData);
      }
    }

    Clients.prototype = {

      setData:function(clientsData){
        var array = [];

        if(clientsData){
          clientsData.forEach(function(clientData){
            array.push(new Client(clientData));
          });
        }

        var clients = {
          array:array
        }

        angular.extend(this, clients)
      },

      /*
      * Set client to the client array in Clients list
      * this method will call after creating new client.
      * @param Client
      */
      setClient:function(clientData){
        var scope = this;
        
        if(clientData instanceof Client){
          scope.array.push(clientData);
        }
      },

      /*
      * Since we need to provide array of object for autocomplete-alt
      * we need attach it this way.
      */
      setDataForSuggest:function(clientsData){
        var scope = this;
        scope.clientsArray = clientsData
        angular.extend(this, scope);
      },

      getAll:function(){
        var scope = this;

        httpService.get('clients/getall')
        .then(function(response){
          scope.setData(response);
        }).catch(function(response){
          console.error(response);
        });
      },

      getForSuggest:function(){
        var scope = this;

        httpService.get('clients/getall')
        .then(function(response){
          scope.setDataForSuggest(response);
        }).catch(function(response){
          console.error(response);
        });
      }

    }

    return Clients;
  }
])
