app.factory('Client', [
  '$q',
  'PeopleProfile',
  'httpService',
  'alertService',
  'config',
  function($q, PeopleProfile, httpService, alertService, config){

    function Client(clientData){
      if(clientData){
        this.setData(clientData);
      }
    }

    Client.prototype = {
      setData: function(clientData){
        angular.extend(this, clientData);
      },

      setPeople:function(people){
        var scope = this;
        scope.people = [];

        if(people){
          people.forEach(function(person){
            scope.people.push(new PeopleProfile(person));
          })
        }
        angular.extend(this,scope);
      },

      setPerson:function(person){
        var scope = this;

        if(person){
          if(!scope.people){
            scope.people = [];
          }
          scope.people.push(new PeopleProfile(person));
        }
      },

      get:function(clientId){
        var scope = this;
        var params = {
          clientId:clientId,
        }

        httpService.get('client/get', params)
        .then(function(response){
          scope.setData(response);
        }).catch(function(response){
          console.error(response);
        });
      },

      create:function(){
        var scope = this;
        return $q(function(resolve, reject){
          httpService.post('client/create', scope)
          .then(function(response){
            if(response.status === 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.CREATE_ITEM, "createClient");
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, "createClient");
              resolve(false);
            }
            scope.setData(response);
          }).catch(function(response){
            console.error(response);
            reject(response);
          });
        });
      },

      update:function(){
        var scope = this;

        return $q(function(resolve, reject){
          httpService.post('client/update', scope)
          .then(function(response){
            if(response.status === 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.UPDATE_ITEM, 'updateClient');
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.UPDATE_ITEM, 'updateClient');
              resolve(false);
            }
            scope.setData(response);
          }).catch(function(response){
            alertService.setAlert('danger', config.ERROR_MSG.UPDATE_ITEM, 'updateClient');
            console.error(response);
            reject(response);
          });
        });
      },

      delete:function(){
        var scope = this;

        return $q(function(resolve, reject){
          httpService.delete('client/delete', scope)
          .then(function(response){
            if(response.status == 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.DELETE_ITEM, 'viewClients');
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.DELETE_ITEM, 'viewClients');
              resolve(false);
            }
          }).catch(function(response){
            console.error(response);
            alertService.setAlert('danger', config.ERROR_MSG.DELETE_ITEM, 'viewClients');
            reject(response);
          });
        });
      },

      getPeople:function(clientId){
        var scope = this;

        var param = {
          type:'client',
          id:clientId
        }

        httpService.get('people/getall', param)
        .then(function(response){
          scope.setPeople(response);
        }).catch(function(response){
          console.error(response);
        });
      },
    }

    return Client;
  }
])
