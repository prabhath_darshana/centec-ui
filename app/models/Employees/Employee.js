app.factory('Employee', ['$q', 'httpService', 'alertService', 'PeopleProfile', 'EmpCategory', 'config',
function($q, httpService, alertService, PeopleProfile, EmpCategory, config){

  function Employee(employeeData){
    if(employeeData){
      this.setData(employeeData);
    }
  }

  Employee.prototype = {
    setData:function(employeeData){

      var profile = new PeopleProfile();
      var category = new EmpCategory();

      if(employeeData.profile){
        profile.setData(employeeData.profile);
      }
      if(employeeData.category){
        category.setData(employeeData.category);
      }

      var employee = {
        id:employeeData.id,
        profile:profile,
        category:category,
        pay_per_day:employeeData.pay_per_day,
        balance_payment:employeeData.balance_payment
      }

      angular.extend(this, employeeData);
    },

    get:function(employeeId){
      var scope = this;

      var params = {
        employeeId:employeeId,
      }

      httpService.get('employee/get', params)
      .then(function(response){
        scope.setData(response);
      }).catch(function(response){
        console.error(response);
      });
    },

    create:function(){
      var scope = this;

      return $q(function(resolve, reject){
        httpService.post('employee/create', scope)
        .then(function(response){
          if(response.status == 'success'){
            alertService.setAlert('success', config.SUCCESS_MSG.CREATE_ITEM, "createEmployee");
            resolve(true);
          }else{
            alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, "createEmployee");
            resolve(false);
          }
          scope.setData(response);
        }).catch(function(response){
          alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, "createEmployee");
          reject(response);
        });
      });
    },

    delete:function(){
      var scope = this;

      return $q(function(resolve, reject){
        httpService.delete('employee/delete', scope)
        .then(function(response){
          if(response.status=='success'){
            alertService.setAlert('success', config.SUCCESS_MSG.DELETE_ITEM, "viewEmployee");
            resolve(true);
          }else{
            alertService.setAlert('danger', config.ERROR_MSG.DELETE_ITEM, "viewEmployee");
            resolve(false);
          }
        }).catch(function(response){
          console.error(response);
          alertService.setAlert('danger', config.ERROR_MSG.DELETE_ITEM, "viewEmployee");
          reject(response);
        });
      });
    }
  }
  return Employee;
}])
