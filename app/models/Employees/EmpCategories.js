app.factory('EmpCategories', [
  'httpService',
  'EmpCategory',
  function(httpService, EmpCategory){
    function EmpCategories(categories){
      this.setData(categories);
    }

    EmpCategories.prototype = {
      setData:function(categoriesData){
        var categories = [];

        if(categoriesData){
          categoriesData.forEach(function(categoryData){
            categories.push(new EmpCategory(categoryData));
          });
        }

        angular.extend(this, categories);
      },

      getAll:function(){
        var scope = this;

        httpService.get('employee/category/getall')
        .then(function(response){
          scope.setData(response);
        }).catch(function(response){
          console.error(response);
        });
      },
    }
    return EmpCategories;
  }
])
