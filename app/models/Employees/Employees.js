app.factory('Employees', [
  '$q',
  'Employee',
  'httpService',
  'config',
  function($q, Employee, httpService, config){

    function Employees(employeesData){
      this.setData();
    }

    Employees.prototype = {
      setData:function(employeesData){
        var array = [];

        if(employeesData){
          employeesData.forEach(function(employee){
            array.push(new Employee(employee));
          });
        }

        var employees = {
          array:array,
        }

        angular.extend(this, employees);
      },

      setEmployee:function(employeeData){
        var scope = this;

        if(employeeData instanceof Employee){
          scope.array.push(employeeData);
        }
      },

      getAll:function(){
        var scope = this;

        return $q(function(resolve, reject){
          httpService.get('employees/getall')
          .then(function(response){
            scope.setData(response);
            resolve(true);
          }).catch(function(response){
            console.error(response);
            reject(response);
          });
        });
      }
    }
    return Employees;
  }
]);
