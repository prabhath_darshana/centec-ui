app.factory('EmpWorkLog', [
  'httpService',
  'EmpWork',
  function(httpService, EmpWork){
    function EmpWorkLog(data){
      if(data){
        this.setData(data);
      }
    }

    EmpWorkLog.prototype = {
      setData:function(data){
        var empWorks = [];
        if(data){
          data.forEach(function(work){
            work.project = {
              id:work.projectId,
              name:work.projectName
            };
            empWorks.push(new EmpWork(work));
          });
        }

        var workLog = {
          works:empWorks
        }

        angular.extend(this, workLog);
      },

      load:function(params){
        var scope = this;

        httpService.get('employee/work/log', params)
        .then(function(response){
          scope.setData(response);
        }).catch(function(response){
          console.error(response);
        });
      }
    }

    return EmpWorkLog;
  }
])
