app.factory('EmpWork', [
  '$q',
  'httpService',
  'alertService',
  'config',
  function($q, httpService, alertService, config){
    function EmpWork(empWorkData){
      if(empWorkData){
        this.setData(empWorkData);
      }
    }

    EmpWork.prototype = {
      setData:function(empWorkData){
        var date = new Date(empWorkData.date);
        var work = {
          id:empWorkData.id,
          project:empWorkData.project,
          work_description:empWorkData.work_description,
          daily_payable:empWorkData.daily_payable,
          day_paid:empWorkData.day_paid,
          payment_completed:empWorkData.payment_completed,
          date:date
        }
        angular.extend(this, work);
      },

      create:function(){
        var scope = this;

        return $q(function(resolve, reject){
          httpService.post('employee/work/create', scope)
          .then(function(response){
            if(response.status === 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.CREATE_ITEM, 'logWork');
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, 'logWork');
              resolve(false);
            }
            scope.setData(response);
          }).catch(function(response){
            alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, 'logWork');
            console.error(response);
            reject(response);
          });
        });
      },

      update:function(){
        var scope = this;

        return $q(function(resolve, reject){
          httpService.put('employee/work/update', scope)
          .then(function(response){
            if(response.status === 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.UPDATE_ITEM, 'logWork');
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.UPDATE_ITEM, 'logWork');
              resolve(false);
            }
            scope.setData(response);
          }).catch(function(response){
            alertService.setAlert('danger', config.ERROR_MSG.UPDATE_ITEM, 'logWork');
            console.error(response);
            reject(response);
          });
        });
      },

      delete:function(){
        var scope = this;

        return $q(function(resolve, reject){
          httpService.delete('employee/work/delete', scope)
          .then(function(response){
            if(response.status === 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.DELETE_ITEM, 'logWork');
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.DELETE_ITEM, 'logWork');
              resolve(false);
            }
            scope.setData(response);
          }).catch(function(response){
            alertService.setAlert('danger', config.ERROR_MSG.DELETE_ITEM, 'logWork');
            console.error(response);
            reject(response);
          });
        });
      },

      dopayment:function(){
        var scope = this;

        return $q(function(resolve, reject){
          httpService.put('employee/work/dopayment', scope)
          .then(function(response){
            if(response.status === 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.UPDATE_ITEM, 'logWork');
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.UPDATE_ITEM, 'logWork');
              resolve(false);
            }
            scope.setData(response);
          }).catch(function(response){
            alertService.setAlert('danger', config.ERROR_MSG.UPDATE_ITEM, 'logWork');
            console.error(response);
            reject(response);
          });
        });
      }
    }
    return EmpWork;
  }
])
