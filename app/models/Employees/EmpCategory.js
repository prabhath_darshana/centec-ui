app.factory('EmpCategory', [
  '$q',
  'httpService',
  'alertService',
  'config',
  function($q, httpService, alertService, config){
    function EmpCategory(empCategoryData){
      this.setData(empCategoryData);
    }

    EmpCategory.prototype = {
      setData:function(empCategoryData){
        var categories = [];

        angular.extend(this, empCategoryData);
      },

      create:function(){
        var scope = this;

        return $q(function(resolve, reject){
          httpService.post('employee/category/create', scope)
          .then(function(response){
            if(response.status == 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.CREATE_ITEM, 'empCategory');
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, 'empCategory');
              resolve(false);
            }
            scope.setData(response);
          }).catch(function(response){
            alertService.setAlert('danger', config.ERROR_MSG.CREATE_ITEM, 'empCategory');
            console.error(response);
            reject(response);
          });
        });
      },

      delete:function(){
        var scope = this;

        return $q(function(resolve, reject){
          httpService.delete('employee/category/delete', scope)
          .then(function(response){
            if(response.status == 'success'){
              alertService.setAlert('success', config.SUCCESS_MSG.DELETE_ITEM, 'empCategory');
              resolve(true);
            }else{
              alertService.setAlert('danger', config.ERROR_MSG.DELETE_ITEM, 'empCategory');
              resolve(false);
            }
            scope.setData(response);
          }).catch(function(response){
            alertService.setAlert('danger', config.ERROR_MSG.DELETE_ITEM, 'empCategory');
            console.error(response);
            reject(response);
          });
        });
      }
    }
    return EmpCategory;
  }
]);
